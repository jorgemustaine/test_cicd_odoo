# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    'name': 'CICD',
    'version': '11.0.1.0.0',
    'license': 'AGPL-3',
    'website': 'https://www.jorgescalona.github.io',
    'summary': 'cicd test ',
    'author': '@jorgemustaine',
    'depends': ['base'],
    'demo': [],
    'data': [],
    'application': True,
    'installable': True,
}
